# Installation
## Python Server
1. Install Docker ```docker run --name mongodb -v /data/db:/data/db -p 27017:27017 -d mongo```
2. Install RabbitMQ ```docker run -d --hostname rabbitmq --name rabbitmq -p 4369:4369 -p 5671-5672:5671-5672 -p 15671-15672:15671-15672 -p 25672:25672 rabbitmq:3-management```
admin page - http://localhost:15672 
default admin/password - guest/guest

