import React from 'react';
import Loadable from 'react-loadable'

import CodeEditors from './views_references/Editors/CodeEditors'
import DefaultLayout from './containers/DefaultLayout';

import 'spinkit/css/spinkit.css';

function Loading() {
  return <div className="sk-spinner sk-spinner-pulse"/>;
}

const Compose = Loadable({
  loader: () => import('./views_references/Apps/Email/Compose'),
  loading: Loading,
});

const Inbox = Loadable({
  loader: () => import('./views_references/Apps/Email/Inbox'),
  loading: Loading,
});

const Message = Loadable({
  loader: () => import('./views_references/Apps/Email/Message'),
  loading: Loading,
});

const Invoice = Loadable({
  loader: () => import('./views_references/Apps/Invoicing/Invoice'),
  loading: Loading,
});

const Breadcrumbs = Loadable({
  loader: () => import('./views_references/Base/Breadcrumbs'),
  loading: Loading,
});

const Cards = Loadable({
  loader: () => import('./views_references/Base/Cards'),
  loading: Loading,
});

const Carousels = Loadable({
  loader: () => import('./views_references/Base/Carousels'),
  loading: Loading,
});

const Collapses = Loadable({
  loader: () => import('./views_references/Base/Collapses'),
  loading: Loading,
});

const Dropdowns = Loadable({
  loader: () => import('./views_references/Base/Dropdowns'),
  loading: Loading,
});

const Jumbotrons = Loadable({
  loader: () => import('./views_references/Base/Jumbotrons'),
  loading: Loading,
});

const ListGroups = Loadable({
  loader: () => import('./views_references/Base/ListGroups'),
  loading: Loading,
});

const Navbars = Loadable({
  loader: () => import('./views_references/Base/Navbars'),
  loading: Loading,
});

const Navs = Loadable({
  loader: () => import('./views_references/Base/Navs'),
  loading: Loading,
});

const Paginations = Loadable({
  loader: () => import('./views_references/Base/Paginations'),
  loading: Loading,
});

const Popovers = Loadable({
  loader: () => import('./views_references/Base/Popovers'),
  loading: Loading,
});

const ProgressBar = Loadable({
  loader: () => import('./views_references/Base/ProgressBar'),
  loading: Loading,
});

const Switches = Loadable({
  loader: () => import('./views_references/Base/Switches'),
  loading: Loading,
});

const Tabs = Loadable({
  loader: () => import('./views_references/Base/Tabs'),
  loading: Loading,
});

const Tooltips = Loadable({
  loader: () => import('./views_references/Base/Tooltips'),
  loading: Loading,
});

const BrandButtons = Loadable({
  loader: () => import('./views_references/Buttons/BrandButtons'),
  loading: Loading,
});

const ButtonDropdowns = Loadable({
  loader: () => import('./views_references/Buttons/ButtonDropdowns'),
  loading: Loading,
});

const ButtonGroups = Loadable({
  loader: () => import('./views_references/Buttons/ButtonGroups'),
  loading: Loading,
});

const Buttons = Loadable({
  loader: () => import('./views_references/Buttons/Buttons'),
  loading: Loading,
});

const LoadingButtons = Loadable({
  loader: () => import('./views_references/Buttons/LoadingButtons'),
  loading: Loading,
});

const Charts = Loadable({
  loader: () => import('./views_references/Charts'),
  loading: Loading,
});

// issue with mispalced position of cm value for acync load
// const CodeEditors = Loadable({
//   loader: () => import('./views_references/Editors/CodeEditors'),
//   loading: Loading,
// });

const TextEditors = Loadable({
  loader: () => import('./views_references/Editors/TextEditors'),
  loading: Loading,
});

const AdvancedForms = Loadable({
  loader: () => import('./views_references/Forms/AdvancedForms'),
  loading: Loading,
});

const BasicForms = Loadable({
  loader: () => import('./views_references/Forms/BasicForms'),
  loading: Loading,
});

const CoreUIIcons = Loadable({
  loader: () => import('./views_references/Icons/CoreUIIcons'),
  loading: Loading,
});

const Flags = Loadable({
  loader: () => import('./views_references/Icons/Flags'),
  loading: Loading,
});

const FontAwesome = Loadable({
  loader: () => import('./views_references/Icons/FontAwesome'),
  loading: Loading,
});

const SimpleLineIcons = Loadable({
  loader: () => import('./views_references/Icons/SimpleLineIcons'),
  loading: Loading,
});

const Alerts = Loadable({
  loader: () => import('./views_references/Notifications/Alerts'),
  loading: Loading,
});

const Badges = Loadable({
  loader: () => import('./views_references/Notifications/Badges'),
  loading: Loading,
});

const Modals = Loadable({
  loader: () => import('./views_references/Notifications/Modals'),
  loading: Loading,
});

const Toastr = Loadable({
  loader: () => import('./views_references/Notifications/Toastr'),
  loading: Loading,
});

const Calendar = Loadable({
  loader: () => import('./views_references/Plugins/Calendar'),
  loading: Loading,
});

const Draggable = Loadable({
  loader: () => import('./views_references/Plugins/Draggable'),
  loading: Loading,
});

const Spinners = Loadable({
  loader: () => import('./views_references/Plugins/Spinners'),
  loading: Loading,
});

const DataTable = Loadable({
  loader: () => import('./views_references/Tables/DataTable'),
  loading: Loading,
});

const Tables = Loadable({
  loader: () => import('./views_references/Tables/Tables'),
  loading: Loading,
});

const Colors = Loadable({
  loader: () => import('./views_references/Theme/Colors'),
  loading: Loading,
});

const Typography = Loadable({
  loader: () => import('./views_references/Theme/Typography'),
  loading: Loading,
});

const Widgets = Loadable({
  loader: () => import('./views_references/Widgets/Widgets'),
  loading: Loading,
});

// ******************************************************************* //
/*
Archanai Custom
 */
// ******************************************************************* //
const Dashboard = Loadable({
  loader: () => import('./components/Dashboard'),
  loading: Loading,
});

const Users = Loadable({
  loader: () => import('./components/Users'),
  loading: Loading,
});

const User = Loadable({
  loader: () => import('./components/Users/User'),
  loading: Loading,
});

const Visitors = Loadable({
  loader: () => import('./components/Visitors'),
  loading: Loading,
});

const Visitor = Loadable({
  loader: () => import('./components/Visitors/Visitor'),
  loading: Loading,
});

const ManageVisitor = Loadable({
  loader: () => import('./components/Visitors/ManageVisitor'),
  loading: Loading,
});

const Training = Loadable({
  loader: () => import('./components/Training'),
  loading: Loading,
});

const Training_Video = Loadable({
  loader: () => import('./components/Training/Video'),
  loading: Loading,
});

const Training_Image = Loadable({
  loader: () => import('./components/Training/Image'),
  loading: Loading,
});

const Training_Info = Loadable({
  loader: () => import('./components/Training/TrainingInfo'),
  loading: Loading,
});

const GoogleMaps = Loadable({
  loader: () => import('./components/GoogleMaps'),
  loading: Loading,
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', name: 'Home', component: DefaultLayout, exact: true },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/theme', name: 'Theme', component: Colors, exact: true },
  { path: '/theme/colors', name: 'Colors', component: Colors },
  { path: '/theme/typography', name: 'Typography', component: Typography },
  { path: '/base', name: 'Base', component: Cards, exact: true },
  { path: '/base/breadcrumbs', name: 'Breadcrumbs', component: Breadcrumbs },
  { path: '/base/cards', name: 'Cards', component: Cards },
  { path: '/base/carousels', name: 'Carousel', component: Carousels },
  { path: '/base/collapses', name: 'Collapse', component: Collapses },
  { path: '/base/dropdowns', name: 'Dropdowns', component: Dropdowns },
  { path: '/base/jumbotrons', name: 'Jumbotrons', component: Jumbotrons },
  { path: '/base/list-groups', name: 'List Groups', component: ListGroups },
  { path: '/base/navbars', name: 'Navbars', component: Navbars },
  { path: '/base/navs', name: 'Navs', component: Navs },
  { path: '/base/paginations', name: 'Paginations', component: Paginations },
  { path: '/base/popovers', name: 'Popovers', component: Popovers },
  { path: '/base/progress-bar', name: 'Progress Bar', component: ProgressBar },
  { path: '/base/switches', name: 'Switches', component: Switches },
  { path: '/base/tabs', name: 'Tabs', component: Tabs },
  { path: '/base/tooltips', name: 'Tooltips', component: Tooltips },
  { path: '/buttons', name: 'Buttons', component: Buttons, exact: true },
  { path: '/buttons/buttons', name: 'Buttons', component: Buttons },
  { path: '/buttons/button-dropdowns', name: 'Dropdowns', component: ButtonDropdowns },
  { path: '/buttons/button-groups', name: 'Button Groups', component: ButtonGroups },
  { path: '/buttons/brand-buttons', name: 'Brand Buttons', component: BrandButtons },
  { path: '/buttons/loading-buttons', name: 'Loading Buttons', component: LoadingButtons },
  { path: '/charts', name: 'Charts', component: Charts },
  { path: '/editors', name: 'Editors', component: CodeEditors, exact: true },
  { path: '/editors/code-editors', name: 'Code Editors', component: CodeEditors },
  { path: '/editors/text-editors', name: 'Text Editors', component: TextEditors },
  { path: '/forms', name: 'Forms', component: BasicForms, exact: true },
  { path: '/forms/advanced-forms', name: 'Advanced Forms', component: AdvancedForms },
  { path: '/forms/basic-forms', name: 'Basic Forms', component: BasicForms },
  { path: '/icons', exact: true, name: 'Icons', component: CoreUIIcons },
  { path: '/icons/coreui-icons', name: 'CoreUI Icons', component: CoreUIIcons },
  { path: '/icons/flags', name: 'Flags', component: Flags },
  { path: '/icons/font-awesome', name: 'Font Awesome', component: FontAwesome },
  { path: '/icons/simple-line-icons', name: 'Simple Line Icons', component: SimpleLineIcons },
  { path: '/notifications', name: 'Notifications', component: Alerts, exact: true },
  { path: '/notifications/alerts', name: 'Alerts', component: Alerts },
  { path: '/notifications/badges', name: 'Badges', component: Badges },
  { path: '/notifications/modals', name: 'Modals', component: Modals },
  { path: '/notifications/toastr', name: 'Toastr', component: Toastr },
  { path: '/plugins', name: 'Plugins', component: Calendar, exact: true },
  { path: '/plugins/calendar', name: 'Calendar', component: Calendar },
  { path: '/plugins/draggable', name: 'Draggable Cards', component: Draggable },
  { path: '/plugins/spinners', name: 'Spinners', component: Spinners },
  { path: '/tables', name: 'Tables', component: Tables, exact: true },
  { path: '/tables/data-table', name: 'Data Table', component: DataTable },
  { path: '/tables/tables', name: 'Tables', component: Tables },
  { path: '/apps', name: 'Apps', component: Compose, exact: true },
  { path: '/apps/email', name: 'Email', component: Compose, exact: true },
  { path: '/apps/email/compose', name: 'Compose', component: Compose },
  { path: '/apps/email/inbox', name: 'Inbox', component: Inbox },
  { path: '/apps/email/message', name: 'Message', component: Message },
  { path: '/apps/invoicing', name: 'Invoice', component: Invoice, exact: true },
  { path: '/apps/invoicing/invoice', name: 'Invoice', component: Invoice },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/visitors', exact: true,  name: 'Visitors', component: Visitors },
  { path: '/visitors/:id', exact: true, name: 'Visitor Details', component: Visitor },
  { path: '/manage_visitor', exact: true, name: 'Manage Visitor', component: ManageVisitor },
  { path: '/training', exact:true, name: 'Model Training', component: Training },
  { path: '/training/video', exact:true, name: 'Model Training Video', component: Training_Video },
  { path: '/training/image', exact:true, name: 'Model Training Image', component: Training_Image },
  { path: '/training/info', exact:true, name: 'Model Training Info', component: Training_Info },
  { path: '/contact', exact:true, name: 'Contact', component: GoogleMaps },
];

export default routes;
