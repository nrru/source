import {
  AUTH_USER,
  UNAUTH_USER,
  AUTH_ERROR,
  FETCH_USERS,
  FETCH_LOGS,
  FETCH_VISITORS,
  SOCKET_STATE,
  VISITOR_ADD_SUCCESS,
  VISITOR_ADD_FAIL,
  VISITOR_DELETE_SUCCESS,
  VISITOR_DELETE_FAIL,
  USER_DELETE_SUCCESS,
  USER_DELETE_FAIL,
  MAKE_PHOTO_SUCCESS,
  MAKE_PHOTO_FAIL,
  RESET_ADD_VISITOR_FORM,
  UPLOAD_PHOTOS_SUCCESS,
  UPLOAD_PHOTOS_FAIL,
  START_REQUEST, SERVER_ERROR, FORM_ERROR,
  TRAINING_FAIL, TRAINING_SUCCESS, START_TRAINING
} from '../actions/types';

const initialStates = {
  visitor_add: '',
  visitor_delete: '',
  photo_make: '',
  photo_upload: '',
  error: '',
  users: [],
  visitors: [],
  isFetching: false
};

export default function (state = initialStates, action) {
  switch (action.type) {
    case AUTH_USER:
      return {...state, isFetching: false, error: '', authenticated: true};
    case UNAUTH_USER:
      return {...state, isFetching: false, authenticated: false};
    case AUTH_ERROR:
      return {...state, isFetching: false, error: action.payload};
    case FETCH_USERS:
      return {...state, isFetching: false, users: action.payload};
    case SOCKET_STATE:
      return {...state, isFetching: false, socket: action.payload};
    case FETCH_LOGS:
      return {...state, isFetching: false, logs: action.payload};
    case FETCH_VISITORS:
      return {...state, isFetching: false, visitors: action.payload};
    case VISITOR_ADD_SUCCESS:
      return {...state, isFetching: false, visitor_add: action.payload};
    case VISITOR_ADD_FAIL:
      return {...state, isFetching: false, visitor_add: action.payload};
    case VISITOR_DELETE_SUCCESS:
      return {...state, isFetching: false, visitor_delete: action.payload};
    case VISITOR_DELETE_FAIL:
      return {...state, isFetching: false, visitor_delete: action.payload};
    case USER_DELETE_SUCCESS:
      return {...state, isFetching: false, user_delete: action.payload};
    case USER_DELETE_FAIL:
      return {...state, isFetching: false, user_delete: action.payload};
    case MAKE_PHOTO_SUCCESS:
      return {...state, isFetching: false, photo_make: action.payload};
    case MAKE_PHOTO_FAIL:
      return {...state, isFetching: false, photo_make: action.payload};
    case UPLOAD_PHOTOS_SUCCESS:
      return {...state, isFetching: false, photo_upload: action.payload};
    case UPLOAD_PHOTOS_FAIL:
      return {...state, isFetching: false, photo_upload: action.payload};
    case RESET_ADD_VISITOR_FORM:
      return {...state, isFetching: false, form_reset: action.payload};
    case START_REQUEST:
      return {...state, isFetching: true};
    case SERVER_ERROR:
      return {...state, isFetching: false, error: action.payload};
    case FORM_ERROR:
      return {...state, isFetching: false, error: action.payload};
    case START_TRAINING:
      return {...state, isFetching: true, training_status: action.payload};
    case TRAINING_SUCCESS:
      return {...state, isFetching: false, training_status: action.payload};
    case TRAINING_FAIL:
      return {...state, isFetching: false, training_status: action.payload};
  }
  return state;
}
