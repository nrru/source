import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import mainReducer from './main_reducer'

const rootReducer = combineReducers({
  form,
  main: mainReducer
});

export default rootReducer;
