import React from 'react';
import {Alert, Checkbox, FormGroup} from 'reactstrap';
import Dropzone from 'react-dropzone';
import * as _ from 'lodash';
import './filepicker.css';
import './dropzone.css';

const FileDropzone = function (field) {
  const files = field.input.value;
  return (
    <div>
      <h5>{field.title}</h5>
      {/* hide the upload drop zone if only a single file */}
      <div hidden={!field.multiple && Array.isArray(files) && files.length > 0}>
        <Dropzone
          accept="image/png, image/jpeg"
          className="filepicker dropzone"
          disabled={field.disabled}
          name={field.name}
          multiple={field.multiple}
          onDrop={(filesToUpload, e) => {
            let newFiles = filesToUpload;
            if (Array.isArray(field.input.value)) {
              newFiles = _.concat(field.input.value, newFiles);
            }
            field.input.onChange(newFiles);
          }}>

          <div>{field.placeholder}</div>

          {field.meta.touched &&
          field.meta.error &&

          <span className="dz-error-message">{field.meta.error}</span>}

          {files && Array.isArray(files) && (
            <div style={{padding: '5px'}} className="dz-preview dz-file-preview">
              <ul className="list-group">
                {
                  files.map((file, i) => <li className="list-group-item" key={i}>
                    <img src={file.preview} className="dz-image"/>
                    <button
                      className="btn btn-xs btn-danger pull-right"
                      disabled={field.disabled}
                      type="button"
                      onClick={(e) => {
                        let y = field.input.value;
                        let remainingFiles = _.filter(y, (o) => o.name !== file.name);
                        field.input.onChange(remainingFiles);
                      }}
                    >
                      <span className="glyphicon glyphicon-remove-sign"/>
                    </button>
                    <span className="dz-filename">{file.name}</span>
                    <div className="pull-right dz-size"
                         style={{paddingRight: '5px'}}>{Math.round(file.size / Math.pow(2, 10)).toLocaleString()} KB
                    </div>
                  </li>)}
              </ul>
            </div>
          )}
        </Dropzone>
      </div>

      <br/>
    </div>
  );
};

export default FileDropzone;
