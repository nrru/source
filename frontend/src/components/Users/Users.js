import React, { Component } from 'react';
import { Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {fetchUsers} from "../../actions";

function UserRow(props) {
  const userLink = `#/users/${props.user.email}`;
  return (
    <tr key={props.index}>
        <td><a href={userLink}>{props.user.firstname}</a></td>
        <td>{props.user.lastname}</td>
        <td>{props.user.email}</td>
    </tr>
  )
}

class Users extends Component {
  componentWillMount() {
    this.props.fetchUsers();
  }

  render() {
    const userList = this.props.users;

    if(!userList) {
      return <p>Loading...</p>
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" md={{ size: 8, offset: 2 }}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"/> Users <small className="text-muted">registered</small>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                    <tr>
                      <th scope="col">Firstname</th>
                      <th scope="col">Lastname</th>
                      <th scope="col">Email</th>
                    </tr>
                  </thead>
                  <tbody>
                  {userList.map((user, index) => <UserRow key={index} user={user}/>)}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
      errorMessage: state.main.error,
      isLoggedIn: state.main.authenticated,
      users: state.main.users,
    };
}

Users = connect(mapStateToProps, {fetchUsers})(Users);

export default withRouter(Users);
