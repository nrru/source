import React, {Component} from 'react';
import {Card, CardHeader, CardBody, NavLink} from 'reactstrap';
import {withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from 'react-google-maps';

const defaultZoom = 8;
const defaultCenter = {lat: 14.615673, lng: 101.388937};
const locations = [
  {
    lat: 14.993233,
    lng: 102.143984,
    label: 'P',
    draggable: false,
    title: 'PPSmart',
    www: '#'
  },
  {
    lat: 13.997435,
    lng: 100.5992063,
    label: 'A',
    draggable: false,
    title: 'Archanai Technology',
    www: '#'
  }
];

class MarkerList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return locations.map((location, index) => {
        return (
          <MarkerWithInfoWindow key={index.toString()} location={location}/>
        )
      }
    );
  }
}

class MarkerWithInfoWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    const {location} = this.props;

    return (
      <Marker onClick={this.toggle} position={location} title={location.title} label={location.label}>
        {this.state.isOpen &&
        <InfoWindow onCloseClick={this.toggle}>
          <NavLink href={location.www} target="_blank">{location.title}</NavLink>
        </InfoWindow>}
      </Marker>
    )
  }
}

const GoogleMapsComponent = withScriptjs(withGoogleMap((props) => {
    return (
      <GoogleMap defaultZoom={defaultZoom} defaultCenter={defaultCenter}>
        {<MarkerList locations={locations}/>}
      </GoogleMap>
    );
  }
));

class ReactGoogleMaps extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  // To use the Google Maps JavaScript API, you must register your app project on the Google API Console and get a Google API key which you can add to your app

  render() {
    return (
    <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <i className="icon-map"/> Maps{' '}
          <a href="" className="badge badge-danger">Archanai Technology</a>
          <div className="card-header-actions">
            <a href="" rel="noreferrer noopener" target="_blank" className="card-header-action">
              <small className="text-muted">Email Us</small>
            </a>
          </div>
        </CardHeader>
        <CardBody>
          <GoogleMapsComponent
            key="map"
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyASyYRBZmULmrmw_P9kgr7_266OhFNinPA"
            loadingElement={<div style={{height: `100%`}}/>}
            containerElement={<div style={{height: `400px`}}/>}
            mapElement={<div style={{height: `100%`}}/>}
          />
        </CardBody>
      </Card>
    </div>
    )
  }
}

export default ReactGoogleMaps;
