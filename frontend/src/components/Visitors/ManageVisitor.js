import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {connect} from 'react-redux';
import {reduxForm, Field} from 'redux-form';
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  Col,
  Form,
  FormFeedback,
  FormGroup,
  Label,
  Input,
  Row,
  FormText, InputGroup, InputGroupAddon, InputGroupText
} from 'reactstrap';
import {addVisitor, resetAddVisitorForm, fetchVisitors} from "../../actions";
import './ManageVisitorForm.css';

import FileDropzone from '../DropZone/FileDropzone';

class ManageVisitor extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.resetAddVisitorForm();
    this.props.fetchVisitors();
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.visitors.length + 1 === nextProps.visitors.length) {
      this.props.resetAddVisitorForm();
      // TODO: EITHER PUSH TO VISITORS LIST OR ADD ANOTHER
      this.props.history.push('/visitors');
    }
  }

  onSubmit = (props) => {
    this.props.addVisitor(props, {files: props.face_images});
    this.props.fetchVisitors();
  };

  onClickFormCancel = () => {
    this.props.resetAddVisitorForm();
  };

  render() {
    return <div className="animated fadeIn">
      <Card>
        <h1>{this.props.errorMessage}</h1> <h1>{this.props.visitorAdded}</h1>
        <h1>{this.props.photoUploaded}</h1>
        <CardHeader>
          <i className="icon-user"/><strong>Add Visitor</strong>
        </CardHeader>
        <CardBody>
          <Row>
            <Col lg="6">
              <Form onSubmit={this.props.handleSubmit(this.onSubmit)} noValidate name='add_visitor_form'>
                <FormGroup>
                  <Label for="firstname">First Name</Label>
                  <Field type="text"
                         name="firstname"
                         id="firstname"
                         component="input"
                         className="form-control"
                         placeholder="First Name"
                         autoComplete="given-name"
                         autoFocus={true}
                         required
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="lastname">Last Name</Label>
                  <Field type="text"
                         name="lastname"
                         id="lastname"
                         className="form-control"
                         component="input"
                         placeholder="Last Name"
                         autoComplete="family-name"
                         required
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="email">Email</Label>
                  <Field type="email"
                         name="email"
                         id="email"
                         className="form-control"
                         component="input"
                         placeholder="Email"
                         autoComplete="email"
                         required
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="dateOfBirth">Date of Birth</Label>
                  <Field type="date"
                         id="dateOfBirth"
                         className="form-control"
                         name="dateOfBirth"
                         component="input"
                         required
                         placeholder="date of birth"
                         autoComplete="date of birth"/>
                </FormGroup>
                <FormGroup>
                  <Label for="phone">Mobile</Label>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText><i className="fa fa-phone"/></InputGroupText>
                    </InputGroupAddon>
                    <Field
                      component="input"
                      className="form-control"
                      id="phone"
                      name="phone"
                      required
                      placeholder="mobile phone"
                      autoComplete="mobile phone"/>
                  </InputGroup>
                  <FormText color="muted">
                    ex. (999) 999-9999
                  </FormText>
                </FormGroup>
                <FormGroup>
                  <Label for="citizen-id">Citizen ID</Label>
                  <InputGroup>
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText><i className="fa fa-male"/></InputGroupText>
                    </InputGroupAddon>
                    <Field
                      component="input"
                      className="form-control"
                      id="citizenID"
                      name="citizenID"
                      required
                      placeholder="Citizen ID"
                      autoComplete="Citizen ID"
                    />
                  </InputGroup>
                  <FormText color="muted">
                    ex. 9-9999-99999-99-9
                  </FormText>
                </FormGroup>
                <FormGroup>
                  <Button
                    type="submit"
                    color="primary"
                    className="mr-1"
                    disabled={false}>
                    Submit
                  </Button>
                  <Button
                    type="reset"
                    color="danger"
                    onClick={
                      this.onClickFormCancel
                    }
                    className="mr-1">
                    Cancel
                  </Button>
                </FormGroup>
              </Form>
            </Col>
            <Col lg="6">
              <Card>
                <CardBody>
                  <FormGroup>
                    <Field
                      title="Add Face Photos"
                      id="face_images"
                      name="face_images"
                      component={FileDropzone}
                      type="file"
                      multiple={true}
                      placeholder="Drag face photos here."
                      className="form-control-file"/>
                  </FormGroup>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.main.error,
    visitors: state.main.visitors,
    isLoggedIn: state.main.authenticated,
    photoUploaded: state.main.photo_upload,
    visitorAdded: state.main.visitor_add
  }
}

ManageVisitor = connect(mapStateToProps, {
  addVisitor,
  resetAddVisitorForm,
  fetchVisitors})(ManageVisitor);

const withReduxForm = reduxForm({
  form: 'add_visitor_form',
  fields: ['firstname', 'lastname', 'email', 'dateOfBirth', 'phone', 'citizenID', 'face_images'],
})(ManageVisitor);

export default withRouter(withReduxForm);
