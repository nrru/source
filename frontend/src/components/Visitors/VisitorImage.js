import React, {Component} from 'react';
import {Fade, Card, CardHeader, Collapse, CardBody, CardImg} from "reactstrap";

class VisitorImage extends Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <div>
        <Fade timeout={1} in={1}>
          <Card>
            <CardHeader>
              <div className="card-header-actions">
                <a href="#" className="card-header-action btn btn-setting"><i className="icon-settings"></i></a>
                <a className="card-header-action btn btn-minimize" data-target="#collapseExample"
                   onClick={() => console.log('click')}><i className="icon-arrow-up"></i></a>
                <a className="card-header-action btn btn-close" onClick={() => console.log('click')}><i
                  className="icon-close"/></a>
              </div>
            </CardHeader>
            <CardImg top width="400px" src={this.props.imagePath}/>
            <Collapse isOpen={true} id="collapseExample">
              <CardBody>
                <p>filename</p>
              </CardBody>
            </Collapse>
          </Card>
        </Fade>
      </div>
    );
  }
}

export default VisitorImage;
