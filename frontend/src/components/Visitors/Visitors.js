import React, {Component} from 'react';
import {Badge, Card, CardBody, CardHeader, Col, Row, Table} from 'reactstrap';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {fetchVisitors} from "../../actions";

function VisitorRow(props) {
  const visitorLink = `#/visitors/${props.visitor.email}`;
  const images = props.visitor.images;
  let firstImageFound = '';
  if (images.length === 0) {
    firstImageFound = 'https://image.flaticon.com/icons/svg/1120/1120410.svg';
  } else {
    firstImageFound = images[0];
  }

  return (
    <tr key={props.index}>
      <td><a href={visitorLink}>{props.visitor.firstname}</a></td>
      <td>{props.visitor.lastname}</td>
      <td>{props.visitor.email}</td>
      <td><img src={firstImageFound} style={{height: '50px'}}/></td>
    </tr>
  )
}

class Visitors extends Component {
  componentWillMount() {
    this.props.fetchVisitors();
  }

  render() {
    const visitorList = this.props.visitors;

    if (!visitorList) {
      return <div className="sk-spinner sk-spinner-pulse"/>;
    }

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" md={{size: 8, offset: 2}}>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"/> Visitors <small className="text-muted">currently in the
                system:</small>
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                  <tr>
                    <th scope="col">Firstname</th>
                    <th scope="col">Lastname</th>
                    <th scope="col">Email</th>
                    <th scope="col">Pic</th>
                  </tr>
                  </thead>
                  <tbody>
                  {visitorList.map((visitor, index) => <VisitorRow key={index} visitor={visitor}/>)}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.main.error,
    isLoggedIn: state.main.authenticated,
    visitors: state.main.visitors,
  };
}

Visitors = connect(mapStateToProps, {fetchVisitors})(Visitors);
export default withRouter(Visitors);
