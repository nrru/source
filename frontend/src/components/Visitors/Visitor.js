import React, {Component} from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
  CardDeck
} from 'reactstrap';
import {connect} from 'react-redux';
import {withRouter, Redirect} from 'react-router-dom';
import {fetchVisitors} from "../../actions";
import VisitorImage from './VisitorImage';

class Visitor extends Component {
  constructor(props) {
    super(props);
    this.drawVisitorImage = this.drawVisitorImage.bind(this);
  }

  componentWillMount() {
    this.props.fetchVisitors();
  }

  drawVisitorImage(image) {
    return <VisitorImage imagePath={image}/>
  }

  render() {
    const visitors = this.props.visitors;

    if (!visitors) {
      return <p>Loading...</p>
    }

    if (visitors.length === 0) {
      return <p>No visitors</p>
    }

    let currentVisitor = visitors.filter(visitor => visitor.email === this.props.match.params.id)[0];

    return (
      <div className="animated fadeIn">
        <Row>
          <Col sm="12" md={{size: 8, offset: 2}}>
            <Card>
              <CardHeader>
                <strong><i className="icon-info pr-1"/>Visitor: {this.props.match.params.id}</strong>
              </CardHeader>
              <CardBody>
                <Table responsive striped hover>
                  <tbody>
                  <tr key={currentVisitor.firstname}>
                    <td>{`${currentVisitor.firstname} ${currentVisitor.lastname}`}</td>
                    <td><strong>{currentVisitor.email}</strong></td>
                  </tr>
                  </tbody>
                </Table>
              </CardBody>
            </Card>
            <CardDeck>
              {currentVisitor.images.map(image => this.drawVisitorImage(image))}
            </CardDeck>
          </Col>
        </Row>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {visitors: state.main.visitors}
}

Visitor = connect(mapStateToProps, {fetchVisitors})(Visitor);

export default withRouter(Visitor);
