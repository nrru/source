import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {signinUser} from "../../../actions";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';

class Login extends Component {
  componentWillMount() {
    this.ifLoggedIn();
  }

  handleFormSubmit({email, password}) {
    this.props.signinUser({email, password})
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      )
    }
  }

  ifLoggedIn() {
    if (this.props.isLoggedIn) {
      this.props.history.push('/');
    }
  }

  render() {
    const {handleSubmit} = this.props;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    {this.renderAlert()}
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Field name="email" component="input" type="email" placeholder="Your Email" className="form-control"/>
                      </InputGroup>

                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"/>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Field name="password" component="input" type="password" placeholder="Your Password" className="form-control"/>
                      </InputGroup>

                      <Row>
                        <Col xs="6">
                          <Button type="submit" color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button color="link" className="px-0">Forgot password?</Button>
                        </Col>
                      </Row>
                    </form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{width: 44 + '%'}}>
                  <CardBody className="text-center">
                    <div>
                      <h2>Sign up</h2>
                      <p>Please sign up for an account with Archanai Technology Smart Camera System.</p>
                      <Button onClick={() => this.props.history.push('/signup') } color="primary" className="mt-3" active>Register Now!</Button>
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    errorMessage: state.main.error,
    isLoggedIn: state.main.authenticated
  };
}

Login = connect(mapStateToProps, {signinUser})(Login);

const withReduxForm = reduxForm({
  form: 'signin_form',
  fields: ['email', 'password']
})(Login);

export default withRouter(withReduxForm)
