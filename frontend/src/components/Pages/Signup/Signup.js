import React, {Component} from 'react';
import {reduxForm, Field} from 'redux-form';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {signupUser} from "../../../actions";
import {
  Button,
  Card,
  CardBody,
  CardFooter,
  Col,
  Container,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row
} from 'reactstrap';

class Signup extends Component {
  handleFormSubmit(formProps) {
    this.props.signupUser(formProps);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      );
    }
  }

  ifLoggedIn() {
    if (this.props.isLoggedIn) {
      this.props.history.push('/');
    }
  }

  render() {
    {this.ifLoggedIn()}
    const {handleSubmit} = this.props;
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <Card className="mx-4">
                <CardBody className="p-4">
                  {this.renderAlert()}
                  <h1>Register</h1>
                  <p className="text-muted">Create your account</p>
                  <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Field name="firstname" component="input" type="text" placeholder="First Name" className="form-control"/>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-cursor"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Field name="lastname" component="input" type="text" placeholder="Last Name" className="form-control"/>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>@</InputGroupText>
                      </InputGroupAddon>
                      <Field name="email" component="input" type="email" placeholder="Email" className="form-control"/>
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Field name="password" component="input" type="password" placeholder="Password" className="form-control"/>
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"/>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Field name="passwordConfirm" component="input" type="password" placeholder="Confirm Password" className="form-control"/>
                    </InputGroup>
                    <Button color="success" block type="submit">Create Account</Button>
                  </form>
                </CardBody>
                <CardFooter className="p-4">
                  <Row>
                    <Col xs="12" sm="6">
                      <Button className="btn-facebook" block><span>facebook</span></Button>
                    </Col>
                    <Col xs="12" sm="6">
                      <Button className="btn-twitter" block><span>twitter</span></Button>
                    </Col>
                  </Row>
                </CardFooter>
              </Card>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

function validate(formProps) {
  const errors = {};

  if (!formProps.email) {
    errors.email = 'Please enter your email.';
  }

  if (!formProps.password) {
    errors.password = 'Please enter a password';
  }

  if (!formProps.passwordConfirm) {
    errors.passwordConfirm = 'Please enter a password confirmation';
  }

  if (formProps.password !== formProps.passwordConfirm) {
    errors.password = 'Passwords must match';
  }

  return errors;
}

function mapStateToProps(state) {
  return {
    errorMessage: state.main.error,
    isLoggedIn: state.main.authenticated
  };
}

Signup = connect(mapStateToProps, {signupUser})(Signup);

const withReduxForm = reduxForm({
  form: 'signup_form',
  fields: ['email', 'password', 'passwordConfirm', 'firstname', 'lastname'],
  validate
})(Signup);

export default withRouter(withReduxForm);
