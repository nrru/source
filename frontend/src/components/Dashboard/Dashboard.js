import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';
import {Col, Row} from 'reactstrap';
import Stream from '../Stream/Stream';

class Dashboard extends Component {
  render() {
    return <div className="animated fadeIn">
      <Row>
        <Col>
          <Stream/>
        </Col>
      </Row>
    </div>;
  }
}

function mapStateToProps(state) {
  return {
    isLoggedIn: state.main.authenticated
  };
}

Dashboard = connect(mapStateToProps,)(Dashboard);

export default withRouter(Dashboard);
