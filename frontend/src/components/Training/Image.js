import React, {Component} from 'react';
import {Row, Col, Card, CardHeader, CardBody} from "reactstrap";
import LaddaButton, {CONTRACT_OVERLAY} from 'react-ladda';

import 'ladda/dist/ladda-themeless.min.css';
import {connect} from 'react-redux';
import {fetchVisitors, startTraining} from "../../actions";

class ImageTraining extends Component {
  constructor(props) {
    super(props);

    this.state = {
      expOverlay: false,
    }
  }

  toggle(name) {
    this.setState({
      [name]: !this.state[name],
      progress: 0.5
    });
    setTimeout(() => this.setState({expOverlay: false}), 2000);
    this.props.startTraining();
    if(this.ws.readyState) {
      this.ws.send(process.env.REACT_APP_WS_START_TRAINING);
    }
  }

  componentWillMount() {
    this.props.fetchVisitors();
    this.ws = new WebSocket(process.env.REACT_APP_ENDPOINT_TRAIN_MODEL);

  }

  componentDidMount() {
    this.ws.onopen = () => {
      console.log('client training socket on');
      this.ws.onmessage = msg => {
        console.log(`server resposne: ${msg.data}`);
        // TODO: if message = finished training -> ws.close()
      };
    };
  }

  componentWillUnmount() {
    this.ws.close();
  }

  render() {
    if (this.props.visitors.length === 0) {
      return <div className="sk-spinner sk-spinner-pulse"/>;
    }
    return <div className="animated fadeIn">
      <Card>
        <CardHeader>
          <i className="icon-cursor"/> Training AI Model - Images{' '}
          <a href="#training/info" className="badge badge-danger">Warning: Extensive GPU Usage</a>
          <div className="card-header-actions">
            <a href="#/docs" rel="noreferrer noopener" target="_blank" className="card-header-action">
              <small className="text-muted">docs</small>
            </a>
          </div>
        </CardHeader>
        <CardBody>
          <p>
            Training AI Model for visitors listed below. This process will take several minutes depending upon amount of
            dataset and server's capability.
          </p>
          <Row className="text-center">

            <Col sm="12" md={{size: 8, offset: 2}}>
              <h6>Click To Start Training Process.</h6>
              <LaddaButton
                className="btn btn-danger btn-ladda"
                loading={this.state.expOverlay}
                onClick={() => this.toggle('expOverlay')}
                data-color="red"
                data-style={CONTRACT_OVERLAY}
              >
                Train!
              </LaddaButton>
            </Col>
          </Row>
        </CardBody>
      </Card>
    </div>
  }
}

function mapStateToProps(state) {
  return {visitors: state.main.visitors}
}

export default connect(mapStateToProps, {fetchVisitors, startTraining})(ImageTraining);

