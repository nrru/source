import axios from 'axios';
import {reset} from 'redux-form';
import {
  AUTH_ERROR,
  AUTH_USER,
  UNAUTH_USER,
  FETCH_USERS,
  VISITOR_ADD_FAIL,
  VISITOR_ADD_SUCCESS,
  USER_DELETE_FAIL,
  USER_DELETE_SUCCESS,
  SOCKET_STATE, FETCH_VISITORS,
  UPLOAD_PHOTOS_SUCCESS,
  UPLOAD_PHOTOS_FAIL,
  START_REQUEST,
  SERVER_ERROR,
  FORM_ERROR,
  RESET_ADD_VISITOR_FORM,
  START_TRAINING,
  TRAINING_FAIL,
  TRAINING_SUCCESS
} from "./types";

import history from '../history';

const ROOT_URL = 'http://localhost:3090';

export function signinUser({email, password}) {
  return function (dispatch) {
    // Submit email/password to the server
    axios.post(`${ROOT_URL}/signin`, {email, password})
      .then(response => {
        // If request is good
        // - update state to indicate user is authenticated
        dispatch({type: AUTH_USER});
        // - save jwt token and id
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('id', response.data.id);
        // redirect to another route
        history.push('/');
      })
      .catch((e) => {
        // If request bad
        // - show an error to the user
        dispatch(authError('Invalid email or password'));
      });

    // If request is successful
  }
}

export function signupUser({email, password, firstname, lastname}) {
  return function (dispatch) {
    axios.post(`${ROOT_URL}/signup`, {email, password, firstname, lastname})
      .then(response => {
        dispatch({type: AUTH_USER});
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('id', response.data.id);
        history.push('/');
      })
      .catch(response => {
        dispatch(authError(response.response.data.error));
      });
  }
}

export function authError(error) {
  return {
    type: AUTH_ERROR,
    payload: error
  }
}

export function signoutUser() {
  localStorage.removeItem('token');
  localStorage.removeItem('id');

  return {type: UNAUTH_USER};
}

export function fetchUsers() {
  return function (dispatch) {
    axios.get(`${ROOT_URL}/users`, {
      headers: {authorization: localStorage.getItem('token')}
    })
      .then(response => {
        dispatch({
          type: FETCH_USERS,
          payload: response.data.users
        });
      });
  }
}

export function deleteUser(id) {
  return function (dispatch) {
    axios.delete(`${ROOT_URL}/delete/${id}`)
      .then(response => {
        dispatch({
          type: USER_DELETE_SUCCESS,
          payload: 'user deleted successfully'
        });
        console.log("User deleted");
      })
      .catch(reason => {
        dispatch({
          type: USER_DELETE_FAIL,
          payload: 'User deleted fail'
        });
        console.log("Can't delete a user.");
        console.log(reason);
      });
  }
}

export function fetchVisitors() {
  return function (dispatch) {
    axios.get(`${ROOT_URL}/visitors`, {
      headers: {authorization: localStorage.getItem('token')}
    })
      .then(response => {
        dispatch({
          type: FETCH_VISITORS,
          payload: response.data.visitors
        });
      });
  }
}

export function addVisitor(formProps, {files}) {
  const {
    firstname,
    lastname,
    email,
    phone,
    citizenID,
    dateOfBirth,
    face_images
  } = formProps;

  let formData = new FormData();
  // Set form data to include 'foldername' key to parse it on server (in Manage_Visitor_Photo_Upload)
  // Set form data to include 'email' key to use it to match record in db when saving files
  formData.set('foldername', `${firstname}_${lastname}`);
  formData.set('email', email);
  try {
    files.map(file => formData.append('file', file));
  } catch (e) {
    return function (dispatch) {
      dispatch({
        type: FORM_ERROR,
        payload: "All fields are required."
      })
    }
  }

  return async function (dispatch) {
    dispatch({type: START_REQUEST});
    try {
      await axios.post(`${ROOT_URL}/add_visitor`, {
        firstname,
        lastname,
        email,
        phone,
        citizenID,
        dateOfBirth,
        face_images
      })
        .then(response => {
          // console.log(response.data.success);
          dispatch({
            type: VISITOR_ADD_SUCCESS,
            payload: response.data.success
          });

          // If visitor created successfully, then upload pictures
          axios.post(`${ROOT_URL}/upload_photos`, formData)
            .then(response => {
              dispatch({
                type: UPLOAD_PHOTOS_SUCCESS,
                payload: response.data.success
              })
            }).catch(reason => {
            dispatch({
              type: UPLOAD_PHOTOS_FAIL,
              // payload: reason.response.data.error
              payload: 'upload failed, this is temp message, fix it on server --> res.send(shit)'
            })
          });
          // Catch if try not successful
        }).catch(reason => {
          // console.log(reason.response.data.error);
          dispatch({
            type: SERVER_ERROR,
            payload: reason.response.data.error
          })
        });
    } catch (e) {
      dispatch({
        type: SERVER_ERROR,
        payload: 'Unknown server error. Please report admin.'
      })
    }
  }
}

export function addSocketToState(ws) {
  const {url} = ws;
  return {
    type: SOCKET_STATE,
    payload: {socket_url: url}
  };
}

export function resetAddVisitorForm() {
  return function (dispatch) {
    dispatch(reset('add_visitor_form'));
  }
}

export function startTraining() {
  return async function (dispatch) {
    dispatch({type: START_TRAINING});
  }
}
