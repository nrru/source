export const AUTH_USER = 'auth_user';
export const UNAUTH_USER = 'unauth_user';
export const AUTH_ERROR = 'auth_error';

export const FETCH_USERS = 'fetch_users';
export const USER_DELETE_SUCCESS = 'user_delete_success';
export const USER_DELETE_FAIL = 'user_add_fail';

export const FETCH_MESSAGE = 'fetch_message';
export const FETCH_LOGS = 'fetch_logs';
export const FETCH_VISITORS = 'fetch_visitors';

export const SOCKET_STATE = 'add_socket_to_state';

export const MAKE_PHOTO_SUCCESS = 'make_photo_success';
export const MAKE_PHOTO_FAIL = 'make_photo_fail';

export const UPLOAD_PHOTOS_SUCCESS = 'upload_photos_success';
export const UPLOAD_PHOTOS_FAIL = 'upload_photos_fail';

export const VISITOR_ADD_SUCCESS = 'visitor_add_success';
export const VISITOR_ADD_FAIL = 'visitor_add_fail';
export const VISITOR_DELETE_SUCCESS = 'visitor_delete_success';
export const VISITOR_DELETE_FAIL = 'visitor_delete_fail';

export const RESET_ADD_VISITOR_FORM = 'reset_ADD_VISITOR_form';

export const START_REQUEST = 'start_request';

export const SERVER_ERROR = 'server_error';

export const FORM_ERROR = 'form_error';

export const START_TRAINING = 'start_training';
export const TRAINING_SUCCESS = 'training_success';
export const TRAINING_FAIL = 'training_fail';
