import React, {Component} from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

export default function (ComposedComponent) {
  class Authentication extends Component {
    componentWillMount() {
      if(!this.props.isLoggedIn) {
        this.props.history.push('/signin');
      }
    }
    componentWillUpdate(nextProps) {
      if(!nextProps.isLoggedIn) {
        this.props.history.push('/signin');
      }
    }
    render() {
      return <ComposedComponent {...this.props}/>;
    }
  }
  function mapStateToProps(state) {
    return {isLoggedIn: state.main.authenticated};
  }
  return withRouter(connect(mapStateToProps)(Authentication));
}
