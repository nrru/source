import './polyfill'
import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import reduxThunk from 'redux-thunk';

import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import rootReducer from './reducers';
import {AUTH_USER, UNAUTH_USER} from "./actions/types";

require('dotenv').config();

const store = createStore(
  rootReducer,
  composeWithDevTools(applyMiddleware(reduxThunk))
);

const token = window.localStorage.getItem('token');

// TODO: for some reason token returns 'null' string if it's unset, may need to double check
if(token === 'null' || token === null || token === undefined) {
  store.dispatch({ type: UNAUTH_USER})
} else {
  store.dispatch({ type: AUTH_USER});
}

ReactDOM.render(
  <Provider store = { store }>
    <App />
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
