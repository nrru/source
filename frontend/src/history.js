import createHistory from 'history/createHashHistory';
const history = createHistory({
  hashType: "noslash" // Omit the leading slash
});
export default history;
