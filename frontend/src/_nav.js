export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer",
      badge: {
        variant: "danger",
        text: "Live!"
      }
    },
    {
      title: true,
      name: "Train Models",
      wrapper: {
        element: "",
        attributes: {}
      }
    },
    {
      name: "Video",
      url: "/training/video",
      icon: "icon-camrecorder"
    },
    {
      name: "Images",
      url: "/training/image",
      icon: "icon-picture"
    },
    {
      name: "Trained Models",
      url: "/training/info",
      icon: "icon-drop"
    },
    {
      divider: true
    },
    {
      title: true,
      name: "Visitors"
    },
    {
      name: "All Visitors",
      url: "/visitors",
      icon: "icon-people"
    },
    {
      name: "Add Visitor",
      url: "/manage_visitor",
      icon: "icon-plus"
    },
    {
      name: "Detected",
      url: "#",
      icon: "icon-user"
    },
    {
      divider: true
    },
    {
      title: true,
      name: "Settings"
    },
    {
      name: "Stream",
      url: "#",
      icon: "icon-settings"
    },
    {
      name: "Sensitivity",
      url: "#",
      icon: "icon-wrench"
    },
    {
      name: "Detection Speed",
      url: "#",
      icon: "icon-equalizer"
    },
    {
      divider: true
    },
    {
      title: true,
      name: "Notifications"
    },
    {
      name: "Socials",
      url: "#",
      icon: "icon-globe",
      children: [
        {
          name: "Line",
          url: "#",
          icon: "icon-bell"
        },
        {
          name: "Facebook",
          url: "#",
          icon: "icon-social-facebook"
        },
        {
          name: "Google",
          url: "#",
          icon: "icon-social-google"
        }
      ]
    },
    {
      name: "SMS",
      url: "#",
      icon: "icon-phone"
    },
    {
      divider: true
    },
    {
      title: true,
      name: "Reports"
    },
    {
      name: "Users Detection",
      url: "/reports",
      icon: "icon-pie-chart"
    },
    {
      name: "Timesheets",
      url: "#",
      icon: "icon-calendar"
    },
    {
      name: "Bandwidth",
      url: "/bandwidth",
      icon: "icon-chart"
    },
    {
      name: "Status",
      url: "#",
      icon: "icon-screen-desktop",
      class: "mt-auto",
      variant: "danger",
      badge: {
        variant: "warning",
        text: "CPU | GPU"
      }
    }
  ]
};
